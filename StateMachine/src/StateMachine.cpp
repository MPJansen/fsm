// StateMachine.cpp : This file contains the 'main' function. Program execution begins and ends there.
//


#include <iostream>
#include <string_view>
#include <string>

#include <typeinfo>

#include "fsm.h"


using namespace hgg;

struct event_connect { std::string_view address; };
struct event_connected {};
struct event_disconnect {};
struct event_timeout {};

struct state_idle : state_t {
    state_idle() : state_t("IDLE") { std::cout << "Idle\n"; }

    bool on_exit() const { return true; };
};

struct state_connecting : state_t {
    static constexpr int n_max = 3;
    int n = 0;
    std::string address;
    explicit state_connecting(std::string address_) : state_t("CONNECTING"), address(std::move(address_))
    {
        std::cout << "Connecting to: " << address << '\n';
    }
};

struct state_connected : state_t {
    state_connected() : state_t("CONNECTED") { std::cout << "Connected\n"; }
};

using state = std::variant<state_idle, state_connecting, state_connected>;

class connection_fsm : public fsm<connection_fsm, state> {
public:
    
    using fsm::on_event; //Use default overload for unimplemented event

    auto on_event(state_idle&, const event_connect& e) { return state_connecting{ std::string(e.address) }; }
    auto on_event(state_connecting&, const event_connected&) { return state_connected{}; }
    auto on_event(state_connecting& s, const event_timeout&)
    {
        return ++s.n < state_connecting::n_max ? std::nullopt : std::optional<state>(state_idle{});
    }
    auto on_event(state_connected&, const event_disconnect&) { return state_idle{}; };


    //void on_state_change(const state& from, const state& to) {
    //    //std::visit(
        //    [&](auto& from, auto& to) -> void {
        //    std::cout << "from: " << typeid(from).name() << " to: " << typeid(to).name() << '\n';
        //}, from, to);
    //    return std::visit(
    //        overload{
    //            [&](auto& from, auto& to) -> void {
    //            std::cout << "from: " << typeid(from).name() << " to: " << typeid(to).name() << '\n'; }
    //        },
    //        from, to
    //    );
    //};

};

template<typename Fsm, typename... Events>
void dispatch(Fsm& fsm, Events&&... events)
{
    (fsm.dispatch(std::forward<Events>(events)), ...);
}

void sequence_1()
{
    std::cout << "\nSequence #1\n";
    connection_fsm fsm;
    dispatch(fsm, event_connect{"train-it.eu" }, event_connected{}, event_disconnect{});
}

void sequence_2()
{
    std::cout << "\nSequence #2\n";
    connection_fsm fsm;
    dispatch(fsm, event_connect{ "train-it.eu" }, event_timeout{}, event_connected{}, event_disconnect{});
}

void sequence_3()
{
    std::cout << "\nSequence #3\n";
    connection_fsm fsm;
    dispatch(fsm, event_connect{ "train-it.eu" }, event_timeout{}, event_timeout{}, event_timeout{});
}

int main()
{
    sequence_1();
    sequence_2();
    sequence_3();

    std::cout << "Hello World!\n"; 
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
