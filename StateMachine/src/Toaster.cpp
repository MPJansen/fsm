#include "fsm.h"
#include <variant>

using namespace hgg;

struct event_do_toast {
    int time{};
};

struct event_do_bake {
    int temperature{};
};

struct state_oven_toasting {
    int time;
    // on_entry()
    static constexpr std::string_view name = "TOASTING";
    // on_exit()
};

struct state_oven_baking : state_t
{
    const char *const name = "TOASTING";


};

using states_heating = std::variant<state_oven_toasting, state_oven_baking>;

class heating_fsm : public fsm<heating_fsm, states_heating> {

};


struct state_door_open : state_t {};

void f() {
    state_oven_toasting s{ 1 };
}

//using state_oven = std::variant<state_heating, 
