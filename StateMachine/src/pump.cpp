#include <variant>
#include <string_view>
#include <string>
#include <iostream>

#include "fsm.h"

struct event_ping
{
    static constexpr std::string_view message = "BLEEP";
};

struct state_pump_on : state<state_pump_on>
{
    static constexpr std::string_view name = "pump on";

    auto on_event(const event_ping &e)
    {
        std::cout << name << " " << e.message << '\n';
        return true;
    }
};

struct state_pump_off : state<state_pump_off>
{
    static constexpr std::string_view name = "pump off";
    auto on_event(const event_ping &e)
    {
        std::cout << name << " " << e.message << '\n';
        return true;
    }
};

using states_pulse = std::variant<state_pump_off, state_pump_on>;

struct event_pump_toggle
{
};

class state_pulse : public fsm<state_pulse, states_pulse>
{

  public:
    static constexpr std::string_view name = "pump pulse";

    // using fsm::on_event;

    auto on_event(state_pump_off &, const event_pump_toggle &) const noexcept { return state_pump_on{}; }
    auto on_event(state_pump_on &, const event_pump_toggle &) const noexcept { return state_pump_off{}; }

    // static_assert(std::is_same<decltype(get_accepted_events_or_null<state_pulse>()), meta::list<int>>{}, "Int");
    //
    // static_assert(std::is_same<decltype(get_accepted_events_or_null<state_pulse>()),
    // meta::list<event_pump_toggle>>{},
    //    "Not equal pulse");
};

struct state_pump_continuous : state<state_pump_continuous>
{
    static constexpr std::string_view name = "pump continuous";
};

struct state_pump_idle : state<state_pump_continuous>
{
    static constexpr std::string_view name = "pump idle";
};

using states_pump = std::variant<state_pump_idle, state_pump_continuous, state_pulse>;

struct event_pump_continuous
{
};

struct event_pump_idle
{
};

struct event_pump_pulse
{
};

class fsm_pump : public fsm<fsm_pump, states_pump>
{
  public:
    static constexpr std::string_view name = "pump operation";

    // using fsm::on_event;

    auto on_event(state_pump_idle &, const event_pump_continuous &) const noexcept { return state_pump_continuous{}; }
    auto on_event(state_pump_idle &, const event_pump_pulse &) const noexcept { return state_pulse{}; }
    auto on_event(state_pump_continuous &, const event_pump_idle &) const noexcept { return state_pump_idle{}; }
    auto on_event(state_pulse &, const event_pump_idle &) const noexcept { return state_pump_idle{}; }
    auto check_exit() const noexcept
    {
        return std::visit(
            overload{ [](const state_pump_idle &) { return true; }, [](auto &) { return false; } }, get_state());
    }
};

static_assert(detail::supports_on_state_event_v<fsm_pump, state_pump_idle, event_pump_continuous>,
    " Not detected");

struct state_fan_on : state<state_fan_on>
{
    static constexpr std::string_view name = "fan on";
};

struct state_fan_idle : state<state_fan_idle>
{
    static constexpr std::string_view name = "fan idle";
};

using states_fan = std::variant<state_fan_idle, state_fan_on>;

struct event_fan_on
{
};

struct event_fan_idle
{
};

class fsm_fan : public fsm<fsm_fan, states_fan>
{
  public:
    static constexpr std::string_view name = "fan operation";

    // using fsm::on_event;
    auto on_event(state_fan_idle &, const event_fan_on &) const noexcept { return state_fan_on{}; }
    auto on_event(state_fan_on &, const event_fan_idle &) const noexcept { return state_fan_idle{}; }
    auto check_exit() const noexcept
    {
        return std::visit(
            overload{ [](const state_fan_idle &) { return true; }, [](auto &) { return false; } }, get_state());
    }
};

struct state_machine_off : state<state_machine_off>
{
    static constexpr std::string_view name = "machine off";
};

struct state_machine_blink : async_state<state_machine_blink>
{
    static constexpr std::string_view name = "machine blink";

    std::future<void> future{ std::async([]() {
        for (int i = 0; i < 5; ++i) {
            std::cout << "Blink On\n";
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            std::cout << "Blink Off\n";
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
        }
    }) };
};

struct state_machine_on : parallel_state<state_machine_on, fsm_fan, fsm_pump, state_machine_blink>
{
    static constexpr std::string_view name = "machine on";
};

using states_machine = std::variant<state_machine_off, state_machine_on>;

struct event_machine_on
{
};

struct event_machine_off
{
};

class fsm_machine : public fsm<fsm_machine, states_machine>
{

  public:
    static constexpr std::string_view name = "machine operation";

    // using fsm::on_event;

    auto on_event(state_machine_off &, const event_machine_on &) const noexcept { return state_machine_on{}; }
    auto on_event(state_machine_on &, const event_machine_off &) const noexcept { return state_machine_off{}; }
    using fsm::check_state_change;
};
struct A
{
};
static_assert(fsm_machine::accepts_event<event_fan_on>(), "Should accept fan on ");
static_assert(!fsm_machine::accepts_event<A>(), "Shouldnt accept A ");

int main()
{
    static_assert(!detail::supports_on_event_v<fsm_machine, A>, "Shouldnt accept A ");
    static_assert(!detail::supports_on_state_event_v<fsm_machine, std::variant_alternative_t<0, states_machine>, A>,
        "Shouldnt accept A ");

    // fsm_pump s;
    //
    // s.dispatch(event_pump_toggle{});
    // s.dispatch(event_pump_pulse{});
    //
    //
    //
    // s.dispatch(event_pump_toggle{});
    // s.dispatch(event_pump_toggle{});
    //
    // s.dispatch(event_pump_continuous{});
    //
    // s.dispatch(event_pump_idle{});
    // s.dispatch(event_pump_continuous{});
    // s.dispatch(event_pump_idle{});

    fsm_machine s;


    std::cout << s.get_full_name() << '\n';
    s.dispatch(event_machine_on{});
    std::this_thread::sleep_for(std::chrono::seconds(3));
    s.dispatch(event_pump_pulse{});
    s.dispatch(event_fan_on{});
    std::cout << s.get_full_name() << '\n';
    s.dispatch(event_ping{});

        s.dispatch(event_machine_off{});
    s.dispatch(event_pump_idle{});
    s.dispatch(event_fan_idle{});
    std::cout << "try shutdown: \n";
    s.dispatch(event_machine_off{});
    std::cout << s.get_full_name() << '\n';
    std::this_thread::sleep_for(std::chrono::seconds(3));
    std::cout << "try shutdown: \n";
    s.dispatch(event_machine_off{});

    // s.dispatch(A{});
    // s.dispatch(B{});
    // s.dispatch(C{});
    return 0;
}