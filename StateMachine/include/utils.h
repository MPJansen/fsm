#pragma once

#include <type_traits>
#include <utility>
//#include <experimental/type_traits>


template <typename T>
struct crtp
{
    T& underlying() noexcept { return static_cast<T&>(*this); }
    T const& underlying() const noexcept { return static_cast<T const&>(*this); }
};

template<typename... Ts> struct overload : Ts... { using Ts::operator()...; };
template<typename... Ts> overload(Ts...)->overload<Ts...>;


template<std::size_t N, std::size_t... Seq>
  constexpr std::index_sequence<N + Seq ...>
  add(std::index_sequence<Seq...>)
  { return {}; }

template<std::size_t Min, std::size_t Max>
  using make_index_range = decltype(add<Min>(std::make_index_sequence<Max-Min>()));

template <typename...>
using void_t = void;

namespace internal
{
    template <typename V, typename D>
    struct detect_impl
    {
        using value_t = V;
        using type = D;
    };

    template <typename D, template <typename...> class Check, typename... Args>
    constexpr auto detect_check(char)
        -> detect_impl<std::false_type, D>;

    template <typename D, template <typename...> class Check, typename... Args>
    constexpr auto detect_check(int)
        -> decltype(void_t<Check<Args...>>(),
                    detect_impl<std::true_type, Check<Args...>>{});

    template <typename D, typename Void, template <typename...> class Check, typename... Args>
    struct detect : decltype(detect_check<D, Check, Args...>(0)) {};
}

struct nonesuch
{
    nonesuch() = delete;
    ~nonesuch() = delete;
    nonesuch(nonesuch const&) = delete;
    void operator=(nonesuch const&) = delete;
};

template <template< typename... > class Check, typename... Args>
using is_detected = typename internal::detect<nonesuch, void, Check, Args...>::value_t;