#pragma once


// Inspired by:
// https://github.com/mpusz/fsm-variant/blob/master/src/include/mp/fsm.h

#include <optional>
#include <variant>
#include <string_view>
#include <string>
#include <tuple>

#include <future>

#include <iostream>
#include <type_traits>

#include "utils.h"


namespace detail {
template<typename Derived, typename State, typename Event>
using on_state_event_t = decltype(std::declval<Derived>().on_event(std::declval<State &>(), std::declval<Event>()));
template<typename Derived, typename State, typename Event>
using supports_on_state_event = is_detected<on_state_event_t, Derived, State, Event>;
template<typename Derived, typename State, typename Event>
inline constexpr bool supports_on_state_event_v = supports_on_state_event<Derived, State, Event>::value;

template<typename Derived, typename Event>
using on_event_t = decltype(std::declval<Derived>().on_event(std::declval<Event>()));
template<typename Derived, typename Event> using supports_on_event = is_detected<on_event_t, Derived, Event>;
template<typename Derived, typename Event>
inline constexpr bool supports_on_event_v = supports_on_event<Derived, Event>::value;

template<typename Derived, typename Event, typename State = void> constexpr bool handles_event()
{
    return supports_on_state_event_v<Derived, State, Event> || supports_on_event_v<Derived, Event>;
}
}// namespace detail


template<typename Derived> struct state : crtp<Derived>
{
    template<typename Event> auto dispatch(Event && event) noexcept { 
		if constexpr (accepts_event<Event>()) {
			return this->underlying().on_event(event);
		}
        return false;
	}
    auto get_full_name() const { return std::string{ this->underlying().name }; }

    template<typename T = void> bool check_entry() const noexcept { return true; }
    template<typename T = void> bool check_exit() const noexcept { return true; }
    template<typename T = void>
    void on_entry() const noexcept {
        // std::cout << "Entering: " << this->underlying().name << '\n';
        // return true;
    };
    template<typename T = void>
    void on_exit() const noexcept {
        // std::cout << "Exiting: " << this->underlying().name << '\n';
        // return true;
    };

    // template<typename Derived, typename Event>
    template<typename Event> static constexpr bool accepts_event()
    {
        return detail::supports_on_event<Derived, Event>::value;
    }
};

template<typename Derived, typename... States> struct parallel_state : state<Derived>
{
    template<typename Event> auto dispatch(Event &&event) noexcept
    {
        auto maybe_dispatch = [event = event](auto &&state) {
            if constexpr (std::decay_t<decltype(state)>::template accepts_event<Event>()) {
                return state.dispatch(event);
            }
            return false;
        };
        return std::apply([event = event, maybe_dispatch](auto &&... states) { return (maybe_dispatch(states) || ...); }, states_);
    };
    template<typename T = void> auto get_full_name() const
    {
        return std::string(this->underlying().name) + "["
               + std::apply([](auto &&... states) { return ((states.get_full_name() + ", ") + ...); }, states_) + "]";
        ;
    }

    template<typename T = void> bool check_entry() const noexcept
    {
        return std::apply([](auto &&... states) { return (states.check_entry() && ...); }, states_);
    }
    template<typename T = void> bool check_exit() const noexcept
    {
        return std::apply([](auto &&... states) { return (states.check_exit() && ...); }, states_);
    }
    template<typename T = void> void on_entry() noexcept
    {
        return std::apply([](auto &&... states) { (states.on_entry(), ...); }, states_);
    }
    template<typename T = void> void on_exit() noexcept
    {
        return std::apply([](auto &&... states) { (states.on_exit(), ...); }, states_);
    }
    std::tuple<States...> states_;
	//TODO add accepts event

	template<typename Event> static constexpr bool accepts_event()
    {
        return detail::supports_on_event_v<Derived, Event> || (States::template accepts_event<Event>() || ...);
    }
};

template<typename Derived> struct async_state : state<Derived>
{
    template<typename T = void> bool check_exit() const
    {
        return this->underlying().future.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
    }
    template<typename T = void> void on_exit() { return this->underlying().future.wait(); }
};

template<typename Derived, typename StateVariant> class fsm : public crtp<Derived>
{

  public:
    // Access the State
    const StateVariant &get_state() const noexcept { return (state_); }
    void set_state(const StateVariant &state) { state_ = state; }

    auto get_full_name() const
    {
        return std::string(this->underlying().name) + "->"
               + std::visit(overload{ [&](auto &s) { return s.get_full_name(); } }, state_);
    };

    template<typename Event> auto dispatch(const Event &event) noexcept
    {

        auto new_state = std::visit(overload{ [this, event = event](auto &s) -> std::optional<StateVariant> {
            if constexpr (std::decay_t<decltype(s)>::template accepts_event<Event>()) {
                s.dispatch(event);
            }
            if constexpr (detail::supports_on_event_v<Derived, Event>) {
                return this->underlying().on_event(event);
            }
            if constexpr (detail::supports_on_state_event_v<Derived, std::decay_t<decltype(s)>, Event>) {
                return this->underlying().on_event(s, event);
            }
            return {};
        } },
            state_);
        if (new_state) {
            state_ = on_state_change(std::move(state_), std::move(*new_state));
            return true;
        } else {
            return false;
        }
    };
    template<typename FromState, typename ToState>
    auto check_state_change(const FromState &, const ToState &) const noexcept -> bool
    {
        return true;
    };

    template<typename T = void> bool check_entry() const noexcept { return true; };
    template<typename T = void> bool check_exit() const noexcept { return true; };
    template<typename T = void> bool on_entry() const noexcept
    {
        // std::cout << "Entering: " << this->underlying().name << '\n';
        return true;
    };
    template<typename T = void> bool on_exit() const noexcept
    {
        // std::cout << "Exiting: " << this->underlying().name << '\n';
        return true;
    };

    template<typename Event, size_t... Is>
    constexpr bool static accepts_event_(const Event &, std::index_sequence<Is...>)
    {
        return detail::supports_on_event_v<Derived, Event> ||//
               (detail::supports_on_state_event_v<Derived, std::variant_alternative_t<Is, StateVariant>, Event> || ...)
               ||//
               (std::variant_alternative_t<Is, StateVariant>::template accepts_event<Event>() || ...);
    }

    template<typename Event> static constexpr bool accepts_event()
    {
        return accepts_event_(Event{}, std::make_index_sequence<std::variant_size_v<StateVariant>>());
    }

  private:
    auto on_state_change(StateVariant from, StateVariant to) noexcept -> StateVariant
    {
        return std::visit(
            [&](auto &&from, auto &&to) -> StateVariant {
                const auto check_exit = from.check_exit();
                const auto check_entry = to.check_entry();
                const auto check_transition = this->underlying().check_state_change(from, to);

                if (check_entry && check_exit && check_transition) {
                    from.on_exit();
                    std::cout << "Changing state from: " << from.name << " to:" << to.name << "\n";
                    to.on_entry();
                    return std::move(to);
                }
                return std::move(from);
            },
            from,
            to);
    };

    StateVariant state_;
};
