#include "pch.h"
#include <any>
#include <functional>

#include "utils.h"

struct X
{
    void foo(int){};
};

// Our test
template<typename T> using is_addable_impl = decltype(std::declval<T>() + std::declval<T>());

template<typename T> using is_addable = is_detected<is_addable_impl, T>;

template<typename T, typename arg> using is_fooable_impl = decltype(std::declval<T>().foo(std::declval<arg&>()));

template<typename T, typename arg> using is_fooable = is_detected<is_fooable_impl, T, arg>;

TEST(Bla, bla)//
{

    static_assert(is_fooable<X, int>::value, "Should FOO");

	bool b = is_fooable<X, int>::value;
    EXPECT_TRUE(is_addable<int>::value);
    EXPECT_TRUE(b);
}